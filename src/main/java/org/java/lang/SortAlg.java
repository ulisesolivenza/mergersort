package org.java.lang;

import java.util.function.Function;

/**
 * Created by ulises on 17/02/16.
 * Represents a in-memory sorting algorithm
 */
public interface SortAlg<T,R> extends Function<T,R> {


}
